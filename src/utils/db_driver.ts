import { knex, Knex } from 'knex';
import { config } from 'dotenv';
import { RowType } from './csv';

interface DB {
  driver: null | Knex;
  options: Knex.Config;
  schema: string;
}

config()

const { PG_URL } = process.env

const db: DB = {
  driver: null,
  options: {
    client: 'pg',
    connection: PG_URL
  },
  schema: 'salesforce',
}

export function getSql() {
  return knex({ client: 'pg' });
}

export function getDb() {
  if (!db.driver) {
    const driver = knex(db.options);
    driver.on('query-error', error => { throw error; });
    db.driver = driver;
  }

  return db.driver;
}

export const sql = {
  insert(table: string, obj: RowType[]) {
    return getSql()
      .insert(obj)
      .into(`${db.schema}.${table}`)
      .toString();
  }
}

export function killDb() {
  return getDb().destroy();
}

export function runSql(sql: string) {
  return getDb().raw(sql);
}