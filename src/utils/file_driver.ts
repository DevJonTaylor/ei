import { readFile, writeFile } from "fs/promises";

export function rf(path: string): Promise<string> {
  return readFile(path, "utf8");
}

export function wf(path: string, str: string): Promise<void> {
  return writeFile(path, str);
}