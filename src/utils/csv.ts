import { config } from "dotenv";

export type RowType = { [headerName: string]: string };

config();

const { CSV_DELIMITER, CSV_ESCAPE, CSV_PIPE } = process.env;

export function serailizeRow(row: string): string {
  let str = [];
  let isEscaped = false;

  for (let i = 0; i < row.length; i++) {
    const character = row[i];

    if (character === CSV_ESCAPE) isEscaped = !isEscaped;

    str.push(
      !isEscaped && character === CSV_DELIMITER
        ? CSV_PIPE
        : character
    );
  }

  return str.join('');
}

export function csv2obj(csv: string): RowType[] {
  const lines = csv.split('\n');
  const headers = lines[0].split(`${CSV_DELIMITER}`);
  lines.shift();
  return lines
    .filter(line => line)
    .map(row => Object.fromEntries(
      serailizeRow(row)
        .split(`${CSV_PIPE}`)
        .map((column, index) => [headers[index], column])
    ));
}