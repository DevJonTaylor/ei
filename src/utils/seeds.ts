import { resolve } from 'path';
import { csv2obj } from './csv';
import { getDb, killDb, sql } from './db_driver';
import { rf, wf } from './file_driver';
type RowType = { [headerName: string]: string };

const out = console.log;
const err = console.error;

async function createInsert(table: string, rows: RowType[]): Promise<void> {
  try {
    const sqlInsert = sql.insert(table, rows);
    await wf(`./database/seeds/${table}.sql`, sqlInsert);
  } catch (error) {
    return Promise.reject(error);
  }
}

async function execSqlFile(filePath: string): Promise<void> {
  try {
    const sql = await rf(filePath);
    return await getDb().raw(sql);
  } catch (error) {
    return Promise.reject(error);
  }
}

async function csvFromFile(filePath: string): Promise<RowType[]> {
  try {
    const str = await rf(filePath);
    return csv2obj(str);
  } catch (error) {
    return Promise.reject(error);
  }
}

async function seed(table: string): Promise<void> {
  try {
    const csv = await csvFromFile(`./database/seeds/${table}.csv`);
    await createInsert(table, csv);
    out(`${table} sql file created.`);
    await execSqlFile(`./database/seeds/${table}.sql`);
    out(`Seeded ${table} table.`);
  } catch (error) {
    return Promise.reject(error);
  }
}

function dbPath(file: string): string {
  return resolve('database', file);
}

async function getEmployeeInteractionInserts(): Promise<void> {
  const insert = await rf('../employee_interaction.json');
  getDb()
    .insert(JSON.parse(insert))
    .into('employee_interaction');
}

async function start(): Promise<void> {
  console.clear();
  out('Setting up Employee Interactions Database...');
  try {
    await execSqlFile(dbPath('drop.sql'));
    out('Dropped Tables, Functions, and Triggers');

    await execSqlFile(dbPath('create.sql'));
    out('Created Tables');

    await execSqlFile(dbPath('comments.sql'));
    out('Comments Added');

    await execSqlFile(dbPath('alter.sql'));
    out('Altered Tables');

    await execSqlFile(dbPath('functions.sql'));
    out('Functions Created');

    await execSqlFile(dbPath('triggers.sql'));
    out('Triggers Created');

    await seed('coaching_categories');
    await seed('coaching_results');
    await seed('coaching_status');
    await seed('interaction_status');
    await seed('interaction_type');
    await seed('record_type');
    await seed('separation_categories');
    await seed('separation_reasons');
    await seed('type_of_warning');
    await seed('type_of_evaluation');
    const ei = [{"id":1,"Approval_Process__c":true,"Client__c":"PARENT_JC_AGRABAH","Coaching_Issued_to_Employee__c":"2021-08-16 21:34:07","Coaching_Status__c":"'Final - Issued to Employee'","Coaching_Status_Date__c":"2022-06-08 18:30:22","Current_Status__c":"Suspended","Date_of_Infraction__c":"2021-08-25 13:46:11","Director__c":"CONTACT_MICKEY","Eligible_for_Rehire__c":true,"Employee_Contact__c":"CONTACT_MICKEY","Name":"EI-97759","Employee_Number__c":73256,"Evaluation_From__c":"2022-07-29 01:06:03","Evaluation_To__c":"2022-04-11 18:06:51","Interaction_Status__c":"Final Review - Approved HR","Interaction_Type__c":"Coaching","Location__c":"PARENT_JC_AGRABAH","Overall_Rating__c":39,"OwnerId":"CONTACT_MICKEY","Performance_Review_Date__c":"2022-08-05 02:59:58","Primary_Reason_for_Coaching_Category__c":"Disruptive Work Behavior","Reason_for_Coaching__c":"Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.","Reason_for_Coaching_Category__c":"Insubordination","RecordTypeId":"012A00000019nMuIAI","Result__c":"Award / Recognition","Separation_Category__c":"Performance Deficiency","Separation_Date__c":"2022-06-24 04:26:04","Separation_Primary_Reason__c":"Invalid license","Separation_Reason__c":"Abandoned post, Attendance / Tardiness, Client request, Communication / Interpersonal / Problem Solving skills, Death, Dependability / Reliability, Did not pass background check, Education, End of Assignment, eVerify Final Nonconfirmation, Excessive absences / tardiness, Gross misconduct, Invalid license, Job knowledge (including technical skills), Medical reasons, Moving, No Call No Show, Other job, Performance, Personal - Family, Personal Reasons, Policy / Procedure Violation, Poor attitude, Quality / Quantity of work, Reduction in force, Resignation, Retirement / Early Retirement, School, Temporary job ended, Theft, Unable to contact, Unavailable for work, Mutual Agreement, Idle, Transferred out","Supervisor__c":"CONTACT_MICKEY","Suspended_Shifts__c":80,"Suspension_Days__c":63,"Suspension_End__c":"2022-06-06 14:12:21","Suspension_Start__c":"2022-01-03 23:46:19","Suspension_Total_Days__c":78,"Total_Rating__c":73,"Type_of_Evaluation__c":"90-Day","Type_of_Warning__c":"2"}];
    await getDb()
      .insert(ei)
      .into('salesforce.employee_interaction__c');
  } catch (error) {
    return Promise.reject(error);
  }
}

start()
  .then(() => killDb())
  .then(() => out('Done'))
  .catch(error => {
    err(error);
    killDb();
  });