import { BaseTable } from './BaseTable';

jest.mock('knex', () =>
  () => ({
    select: jest.fn().mockReturnThis(),
    from: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    insert: jest.fn().mockReturnThis(),
    update: jest.fn().mockReturnThis(),
    delete: jest.fn().mockReturnThis(),
    then: jest.fn(function (done) {
      done(null);
    })
  })
);

const tableOptions = {
  schema: 'testing',
  name: 'test'
}

describe('Base Table Class', () => {
  it('should be able to instantiate this object.', () => {
    expect(new BaseTable(tableOptions)).toBeTruthy();
  });

  it('should provide insert query for this specific table.', () => {
    const baseTable = new BaseTable(tableOptions);
    expect(baseTable.insert({ name: 'test' }).toSQL().sql)
      .toEqual({});
  });

  it('should provide update query for this specific table.', () => {
    test.todo('test Base update query');
  });

  it('should provide delete query for this specific table.', () => {
    test.todo('test Base delete query');
  });

  it('should provide select query for this specific table.', () => {
    test.todo('test Base select query');
  });
});