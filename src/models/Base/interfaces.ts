import { Knex } from "knex";

// Column value types
export type BaseColumnValue = string | number | boolean | Date | null;

// A row is an Object Literal/Key Value Pair.
export type BaseRow = {
  [column: string]: BaseColumnValue;
}

// The single types that are accepted
export type BaseTypes = BaseRow | BaseModel;


export type BaseTableReturn = BaseTypes | BaseCollection;

/**
 * @description Contains methods for converting to an Object literal and a string.
 * @author Jon Taylor
 * @since 0.0.1
 */
export interface BaseToObjectMethods {
  /**
   * @description Converts the object to an Object literal.
   * @returns { BaseRow }
   * @author Jon Taylor
   * @since 0.0.1
   */
  toObject(): BaseRow;

  /**
   * @description Converts the object to a string in JSON format.
   * @returns { string }
   * @author Jon Taylor
   * @since 0.0.1
   */
  toString(): string;
}

export interface BaseModel extends BaseToObjectMethods {
  columns: BaseRow;

  /**
   * @description Checks if the current data is synced with the database.
   * @returns { boolean }
   * @author Jon Taylor
   * @since 0.0.1
   */
  isSynced(): boolean;

  /**
   * @description Deletes the current row from the database.
   * @returns { Promise<boolean> }
   * @author Jon Taylor
   * @since 0.0.1
   */
  delete(): Promise<boolean>;

  /**
   * @description Updates the current row in the database.
   * @returns { Promise<boolean> }
   * @author Jon Taylor
   * @since 0.0.1
   */
  save(): Promise<boolean>;
}

/**
 * @description A callback used as a wrapper for the Array methods that
 * expect an item returned.
 * @returns { BaseModel }
 */
export type BaseCollectionCallbackModel =
  (row: BaseTypes, index?: number, allRows?: Array<BaseModel>) => BaseModel;

/**
 * @description A callback used as a wrapper for the Array methods expecting
 * a boolean returned.
 */
export type BaseCollectionCallbackTruthy =
  (row: BaseTypes, index?: number, allRows?: Array<BaseModel>) => boolean;

/**
 * @decription Implements the ability to iterate through Models.
 * @author Jon Taylor
 * @since 0.0.1
 */
export interface BaseCollection extends BaseToObjectMethods {
  rows: Array<BaseModel>;

  /**
   * @description Loops through the collection allowing you to perform edits 
   * or checks on each row.  This method mutates the collection directly.  
   * It returns itself for chaining.
   * @param { BaseCollectionCallbackModel } callback
   * @returns { BaseCollection }
   * @author Jon Taylor
   * @since 0.0.1
   */
  forEach(callback: BaseCollectionCallbackModel): BaseCollection;

  /**
   * @description Loops through the collection allowing you to perform edits
   * or checks on each row.  Your callback is expecting a boolean type which 
   * decides which item to keep.  This method creates a new collection with 
   * the filtered rows.
   * @param { BaseCollectionCallbackTruthy } callback
   * @returns { BaseCollection }
   * @author Jon Taylor
   * @since 0.0.1
   */
  filter(callback: BaseCollectionCallbackTruthy): BaseCollection;

  /**
   * @description Loops through the collection allowing you to perform edits
   * or checks on each row.  Your callback should return a row.  If it is a 
   * different object it will try to use it as a model.  If it cannot it will
   * be ignored.
   * @param { BaseCollectionCallbackModel } callback
   * @returns { BaseCollection }
   * @author Jon Taylor
   * @since 0.0.1
   */
  map(callback: BaseCollectionCallbackModel): BaseCollection;
}

export type BaseEventHandler = (...args: Array<
  BaseColumnValue |
  BaseModel |
  BaseCollection |
  Error
>) => void;

/**
 * @description Implements basic CRUD operations for the table it represents.
 * @author Jon Taylor
 * @since 0.0.1
 */
export interface BaseTable {
  name: string;
  columns: BaseRow;

  /**
   * @description Creates a new event listener.  When the event is triggered
   * the eventHandler is called.
   * @param { string } eventName
   * @param { BaseEventHandler } eventHandler
   * @returns { BaseTable }
   * @author Jon Taylor
   * @since 0.0.1
   */
  on(eventName: string, eventHandler: BaseEventHandler): BaseTable;

  /**
   * @description Triggers an event.  Any arguments passed to this method will
   * be passed to the event handler.
   * @param { string } eventName
   * @returns { BaseTable }
   * @author Jon Taylor
   * @since 0.0.1
   */
  emit(eventName: string): BaseTable;

  /**
   * @description Inserts the row or rows passed to this method into the table.
   * A collection is returned.
   * @param { BaseModel } row
   * @returns { Promise<BaseTableReturn> }
   * @author Jon Taylor
   * @since 0.0.1
   */
  insert(row: BaseRow | BaseCollection): Promise<BaseTableReturn>;

  /**
   * @description Updates the rows found with from the where statement.
   * All updated rows are returned in a collection.
   * @param { BaseRow } row
   * @param { BaseRow } where
   * @returns { Promise<BaseTableReturn> }
   * @author Jon Taylor
   * @since 0.0.1
   */
  update(where: BaseRow, newColumnValues: BaseRow): Promise<BaseTableReturn>;

  /**
   * @description Deletes any rows found with the where statements.
   * Number of rows deleted are returned.
   * @param { BaseRow } where
   * @returns { Promise<number> }
   * @author Jon Taylor
   * @since 0.0.1
   */
  delete(where: BaseRow): Promise<number>;

  /**
   * @description Returns all rows found using the columns and where statement.
   * A collection is returned.
   * @param { Array<string> } columns
   * @param { BaseRow } where
   * @returns { Promise<BaseTableReturn> }
   */
  select(columns: string[], where: BaseRow): Promise<BaseTableReturn>;

  /**
   * @description Returns the knex querybuilder if you should need it.
   * @returns { Knex }
   * @author Jon Taylor
   * @since 0.0.1
   */
  getQueryBuilder(): Knex.QueryBuilder;
}