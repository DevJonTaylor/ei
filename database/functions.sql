CREATE OR REPLACE FUNCTION "salesforce"."get_record_type_id"(
  CHAR(18)
) RETURNS INTEGER AS $$
DECLARE "record_type" INTEGER;
BEGIN
    SELECT MAX("id") INTO "record_type" 
    FROM "salesforce"."record_type"
    WHERE "type_id" = $1;

    RETURN "record_type";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_record_type"(
  VARCHAR(30)
)
  RETURNS INTEGER AS $$
DECLARE "record_type" INTEGER;
BEGIN
    SELECT "id" INTO "record_type"
    FROM "salesforce"."record_type"
    WHERE "type" = $1;
    
    RETURN COALESCE("record_type", 0);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_coaching_status"(VARCHAR(40))
  RETURNS INTEGER AS $$
DECLARE "coaching_status" INTEGER;
BEGIN
  SELECT MAX("id") INTO "coaching_status"
  FROM "salesforce"."coaching_status"
  WHERE "status" = $1;

  RETURN "coaching_status";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_interaction_type"(VARCHAR(18))
  RETURNS INTEGER AS $$
DECLARE "interaction_type" INTEGER;
BEGIN
  SELECT MAX("id") INTO "interaction_type"
  FROM "salesforce"."interaction_type"
  WHERE "type" = $1;

  RETURN "interaction_type";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_interaction_status"(VARCHAR(42))
  RETURNS INTEGER AS $$
DECLARE "interaction_status" INTEGER;
BEGIN
  SELECT MAX("id") INTO "interaction_status"
  FROM "salesforce"."interaction_status"
  WHERE "status" = $1;

  RETURN "interaction_status";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_type_of_evaluation"(VARCHAR(8))
  RETURNS INTEGER AS $$
DECLARE "type_of_evaluation" INTEGER;
BEGIN
  SELECT MAX("id") INTO "type_of_evaluation"
  FROM "salesforce"."type_of_evaluation"
  WHERE "type" = $1;

  RETURN "type_of_evaluation";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_type_of_warning"(VARCHAR(7))
  RETURNS INTEGER AS $$
DECLARE "warning_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "warning_id"
  FROM "salesforce"."type_of_warning"
  WHERE "type" = $1;

  RETURN "warning_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_coaching_result"(VARCHAR(28))
  RETURNS INTEGER AS $$
DECLARE "result_id" INTEGER;
BEGIN
SELECT MAX("id") INTO "result_id"
FROM "salesforce"."coaching_results"
WHERE "result" = $1;

RETURN "result_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."last_ei_id"() RETURNS INTEGER AS $$
DECLARE "last_ei_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "last_ei_id"
  FROM "salesforce"."employee_interactions";

  RETURN "last_ei_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."last_coaching_id"() RETURNS INTEGER AS $$
DECLARE "last_coaching_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "last_coaching_id"
  FROM "salesforce"."coachings";

  RETURN "last_coaching_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."last_separation_id"() RETURNS INTEGER AS $$
DECLARE "last_separation_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "last_separation_id"
  FROM "salesforce"."separations";

  RETURN "last_separation_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_separation_reason"(VARCHAR(52))
  RETURNS INTEGER AS $$
DECLARE "separation_category_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "separation_category_id"
  FROM "salesforce"."separation_categories"
  WHERE "category" = $1;

  RETURN "separation_category_id";
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION "salesforce"."get_coaching_category"(VARCHAR(31))
  RETURNS INTEGER AS $$
DECLARE "coaching_category_id" INTEGER;
BEGIN
  SELECT MAX("id") INTO "coaching_category_id"
  FROM "salesforce"."coaching_categories"
  WHERE "category" = $1;

  RETURN "coaching_category_id";
END;
$$ LANGUAGE plpgsql;
