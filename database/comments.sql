COMMENT ON TABLE "salesforce"."primary_coaching_reason_categories_coaching_reason_categories"
IS 'Many to Many Handles both Salesforce: Reason_for_Coaching_Category__c & Primary_Reason_for_Coaching_Category__c';

COMMENT ON TABLE "salesforce"."primary_separation_reasons_separation_reasons"
IS 'Many to Many Handles Salesforce: Separation_Primary_Reason__c & Separation_Reason__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."id"
IS 'Postgres Only';

COMMENT ON COLUMN "salesforce"."employee_interactions"."ei"
IS 'Salesfore: Name';

COMMENT ON COLUMN "salesforce"."employee_interactions"."coaching_status_date"
IS 'Salesforce: Coaching_Status_Date__c | NOTE: The date and time of the last coaching status.';

COMMENT ON COLUMN "salesforce"."employee_interactions"."coaching_issued"
IS 'Salesforce: Coaching_Issued_To_Employee__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."approval_process"
IS 'Salesforce: Approval_Process__c | NOTE: True if this interaction is in an approval process.';

COMMENT ON COLUMN "salesforce"."employee_interactions"."coaching_status"
IS 'Salesforce: Coaching_Status__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."interaction_type"
IS 'Salesforce: Interaction_Type__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."interaction_status"
IS 'Salesforce: Interaction_Status__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."record_type"
IS 'Salesforce: RecordTypeId';

COMMENT ON COLUMN "salesforce"."employee_interactions"."location"
IS 'Salesforce: Location__c | NOTE: This is the location of the incident.  This may not always be the master job code for the employee. This allows for interactions to be specific to the location where the incident took place.';

COMMENT ON COLUMN "salesforce"."employee_interactions"."client"
IS 'Salesforce: Client_c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."owner"
IS 'Salesforce: OwnerId';

COMMENT ON COLUMN "salesforce"."employee_interactions"."employee_contact"
IS 'Salesforce: Employee_Contact__c';

COMMENT ON COLUMN "salesforce"."employee_interactions"."supervisor"
IS 'Salesforce: Supervisor__c';

COMMENT ON COLUMN "salesforce"."coachings"."id"
IS 'Postgres Only';

COMMENT ON COLUMN "salesforce"."coachings"."employee_interaction"
IS 'Postgres ID';

COMMENT ON COLUMN "salesforce"."coachings"."date_of_infraction"
IS 'Salesforce: Date_of_Infraction__c';

COMMENT ON COLUMN "salesforce"."coachings"."reason"
IS 'Salesforce: Reason_for_Coaching__c';

COMMENT ON COLUMN "salesforce"."coachings"."result"
IS 'Salesforce: Result__c';

COMMENT ON COLUMN "salesforce"."coachings"."type_of_warning"
IS 'Salesforce: Type_of_Warning__c';

COMMENT ON COLUMN "salesforce"."coachings"."director"
IS 'Salesforce: Director__c | NOTE: Director from user account Supervisor.  The interaction will to be approved by this user in order to issued to the employee.  This user is  second approver for this interaction.';

COMMENT ON COLUMN "salesforce"."performance_reviews"."id"
IS 'Postgres Only';

COMMENT ON COLUMN "salesforce"."performance_reviews"."employee_interaction"
IS 'Postgres ID';

COMMENT ON COLUMN "salesforce"."performance_reviews"."start_date"
IS 'Salesforce: Evaluation_From__c';

COMMENT ON COLUMN "salesforce"."performance_reviews"."end_date"
IS 'Salesforce: Evaluation_To__c';

COMMENT ON COLUMN "salesforce"."performance_reviews"."review_date"
IS 'Salesforce: Performance_Review_Date__c';

COMMENT ON COLUMN "salesforce"."performance_reviews"."total_rating"
IS 'Salesforce: Total_Rating__c';

COMMENT ON COLUMN "salesforce"."performance_reviews"."type_of_evaluation"
IS 'Salesforce: Type_of_Evaluation__c';

COMMENT ON COLUMN "salesforce"."separations"."coaching"
IS 'Postgres ID';

COMMENT ON COLUMN "salesforce"."separations"."date"
IS 'Salesforce: Separation_Date__c';

COMMENT ON COLUMN "salesforce"."separations"."eligible_for_rehire"
IS 'Salesforce: Eligible_for_Rehire__c';

COMMENT ON COLUMN "salesforce"."separations"."category"
IS 'Salesforce: Separation_Category__c';

COMMENT ON COLUMN "salesforce"."suspensions"."coaching"
IS 'Postgres ID';

COMMENT ON COLUMN "salesforce"."suspensions"."shifts"
IS 'Salesforce: Suspended_Shifts__c';

COMMENT ON COLUMN "salesforce"."suspensions"."start"
IS 'Salesforce: Suspension_Start__c';

COMMENT ON COLUMN "salesforce"."suspensions"."end"
IS 'Salesforce: Suspension_End__c';
