ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("coaching_status")
REFERENCES "salesforce"."coaching_status" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("interaction_type")
REFERENCES "salesforce"."interaction_type" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("interaction_status")
REFERENCES "salesforce"."interaction_status" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("record_type")
REFERENCES "salesforce"."record_type" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("location")
REFERENCES "salesforce"."location_data__c" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("client")
REFERENCES "salesforce"."location_data__c"("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("owner")
REFERENCES "salesforce"."contact" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("employee_contact")
REFERENCES "salesforce"."contact" ("id");

ALTER TABLE "salesforce"."employee_interactions"
ADD FOREIGN KEY ("supervisor")
REFERENCES "salesforce"."contact" ("id");

ALTER TABLE "salesforce"."coachings"
ADD FOREIGN KEY ("employee_interaction")
REFERENCES "salesforce"."employee_interactions" ("id");

ALTER TABLE "salesforce"."coachings"
ADD FOREIGN KEY ("result")
REFERENCES "salesforce"."coaching_results" ("id");

ALTER TABLE "salesforce"."coachings"
ADD FOREIGN KEY ("type_of_warning")
REFERENCES "salesforce"."type_of_warning" ("id");

ALTER TABLE "salesforce"."coachings"
ADD FOREIGN KEY ("director")
REFERENCES "salesforce"."contact" ("id");

ALTER TABLE "salesforce"."performance_reviews"
ADD FOREIGN KEY ("employee_interaction")
REFERENCES "salesforce"."employee_interactions" ("id");

ALTER TABLE "salesforce"."performance_reviews"
ADD FOREIGN KEY ("type_of_evaluation")
REFERENCES "salesforce"."type_of_evaluation" ("id");

ALTER TABLE "salesforce"."separations"
ADD FOREIGN KEY ("coaching")
REFERENCES "salesforce"."coachings" ("id");

ALTER TABLE "salesforce"."separations"
ADD FOREIGN KEY ("category")
REFERENCES "salesforce"."separation_categories" ("id");

ALTER TABLE "salesforce"."suspensions"
ADD FOREIGN KEY ("coaching")
REFERENCES "salesforce"."coachings" ("id");

