CREATE TABLE "salesforce"."employee_interaction__c" (
  "id" SERIAL PRIMARY KEY,
  "Approval_Process__c" BOOLEAN DEFAULT FALSE,
  "Client__c" VARCHAR(18) DEFAULT NULL,
  "Coaching_Issued_to_Employee__c" TIMESTAMP DEFAULT NULL,
  "Coaching_Status__c" VARCHAR(40) DEFAULT NULL,
  "Coaching_Status_Date__c" TIMESTAMP DEFAULT NULL,
  "Current_Status__c" VARCHAR(10),
  "Date_of_Infraction__c" TIMESTAMP DEFAULT NULL,
  "Director__c" VARCHAR(18) DEFAULT NULL,
  "Eligible_for_Rehire__c" BOOLEAN DEFAULT FALSE,
  "Employee_Contact__c" VARCHAR(18) DEFAULT NULL,
  "Name" CHAR(9) NOT NULL,
  "Employee_Number__c" CHAR(5) DEFAULT NULL,
  "Evaluation_From__c" TIMESTAMP DEFAULT NULL,
  "Evaluation_To__c" TIMESTAMP DEFAULT NULL,
  "Interaction_Status__c" VARCHAR(42) DEFAULT NULL,
  "Interaction_Type__c" VARCHAR(18) DEFAULT NULL,
  "Location__c" VARCHAR(18) DEFAULT NULL,
  "Overall_Rating__c" INTEGER DEFAULT NULL,
  "OwnerId" VARCHAR(18) DEFAULT NULL,
  "Performance_Review_Date__c" TIMESTAMP DEFAULT NULL,
  "Primary_Reason_for_Coaching_Category__c" VARCHAR(31) DEFAULT NULL,
  "Reason_for_Coaching__c" TEXT DEFAULT NULL,
  "Reason_for_Coaching_Category__c" TEXT DEFAULT NULL,
  "RecordTypeId" VARCHAR(18) DEFAULT NULL,
  "Result__c" VARCHAR(28) DEFAULT NULL,
  "Separation_Category__c" VARCHAR(28) DEFAULT NULL,
  "Separation_Date__c" TIMESTAMP DEFAULT NULL,
  "Separation_Primary_Reason__c" VARCHAR(54) DEFAULT NULL,
  "Separation_Reason__c" TEXT,
  "Supervisor__c" VARCHAR(18) DEFAULT NULL,
  "Suspended_Shifts__c" INTEGER DEFAULT NULL,
  "Suspension_Days__c" INTEGER DEFAULT NULL,
  "Suspension_End__c" TIMESTAMP DEFAULT NULL,
  "Suspension_Start__c" TIMESTAMP DEFAULT NULL,
  "Suspension_Total_Days__c" INTEGER DEFAULT NULL,
  "Total_Rating__c" INTEGER DEFAULT NULL,
  "Type_of_Evaluation__c" VARCHAR(8) DEFAULT NULL,
  "Type_of_Warning__c" VARCHAR(7) DEFAULT NULL
);

CREATE TABLE "salesforce"."employee_interactions" (
  "id" SERIAL PRIMARY KEY,
  "ei" CHAR(9) UNIQUE NOT NULL,
  "coaching_status_date" TIMESTAMP DEFAULT NULL,
  "coaching_issued" TIMESTAMP DEFAULT NULL,
  "approval_process" BOOLEAN DEFAULT FALSE,
  "coaching_status" INTEGER DEFAULT NULL,
  "interaction_type" INTEGER DEFAULT NULL,
  "interaction_status" INTEGER DEFAULT NULL,
  "record_type" INTEGER NOT NULL,
  "location" VARCHAR(18) DEFAULT NULL,
  "client" VARCHAR(18) DEFAULT NULL,
  "owner" VARCHAR(18) DEFAULT NULL,
  "employee_contact" VARCHAR(18) DEFAULT NULL,
  "supervisor" VARCHAR(18) DEFAULT NULL
);

CREATE TABLE "salesforce"."coachings" (
  "id" SERIAL PRIMARY KEY,
  "employee_interaction" INTEGER NOT NULL,
  "date_of_infraction" TIMESTAMP DEFAULT NULL,
  "reason" text DEFAULT NULL,
  "result" INTEGER DEFAULT NULL,
  "type_of_warning" INTEGER DEFAULT NULL,
  "director" VARCHAR(18) DEFAULT NULL
);

CREATE TABLE "salesforce"."performance_reviews" (
  "id" SERIAL PRIMARY KEY,
  "employee_interaction" INTEGER NOT NULL,
  "start_date" TIMESTAMP DEFAULT NULL,
  "end_date" TIMESTAMP DEFAULT NULL,
  "review_date" TIMESTAMP DEFAULT NULL,
  "total_rating" INTEGER DEFAULT NULL,
  "type_of_evaluation" INTEGER DEFAULT NULL
);

CREATE TABLE "salesforce"."separations" (
  "id" SERIAL PRIMARY KEY,
  "coaching" INTEGER NOT NULL,
  "date" TIMESTAMP DEFAULT NULL,
  "eligible_for_rehire" BOOLEAN DEFAULT TRUE,
  "category" INTEGER DEFAULT NULL
);

CREATE TABLE "salesforce"."suspensions" (
  "id" SERIAL PRIMARY KEY,
  "coaching" INTEGER NOT NULL,
  "shifts" INTEGER DEFAULT NULL,
  "start" TIMESTAMP DEFAULT NULL,
  "end" TIMESTAMP DEFAULT NULL
);

CREATE TABLE "salesforce"."coaching_status" (
  "id" SERIAL PRIMARY KEY,
  "status" VARCHAR(40)
);

CREATE TABLE "salesforce"."interaction_status" (
  "id" SERIAL PRIMARY KEY,
  "status" VARCHAR(42)
);

CREATE TABLE "salesforce"."record_type" (
  "id" SERIAL PRIMARY KEY,
  "type" VARCHAR(30),
  "type_id" VARCHAR(18)
);

CREATE TABLE "salesforce"."type_of_warning" (
  "id" SERIAL PRIMARY KEY,
  "type" VARCHAR(7)
);

CREATE TABLE "salesforce"."interaction_type" (
  "id" SERIAL PRIMARY KEY,
  "type" VARCHAR(18)
);

CREATE TABLE "salesforce"."type_of_evaluation" (
  "id" SERIAL PRIMARY KEY,
  "type" VARCHAR(8)
);

CREATE TABLE "salesforce"."coaching_results" (
  "id" SERIAL PRIMARY KEY,
  "result" VARCHAR(28)
);

CREATE TABLE "salesforce"."separation_categories" (
  "id" SERIAL PRIMARY KEY,
  "category" VARCHAR(28)
);

CREATE TABLE "salesforce"."separation_reasons" (
  "id" SERIAL PRIMARY KEY,
  "reason" VARCHAR(54)
);

CREATE TABLE "salesforce"."coaching_categories" (
  "id" SERIAL PRIMARY KEY,
  "category" VARCHAR(31)
);

CREATE TABLE "salesforce"."primary_coaching_reason_categories_coaching_reason_categories" (
  "coaching" INTEGER NOT NULL,
  "category_reasons" INTEGER NOT NULL,
  "is_primary" BOOLEAN DEFAULT FALSE
);

CREATE TABLE "salesforce"."primary_separation_reasons_separation_reasons" (
  "separation" INTEGER NOT NULL,
  "reason" INTEGER NOT NULL,
  "is_primary" BOOLEAN DEFAULT FALSE
);