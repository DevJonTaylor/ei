CREATE OR REPLACE FUNCTION salesforce.ei_trigger_func()
  RETURNS TRIGGER
  LANGUAGE PLPGSQL
  AS $$
DECLARE separation_node VARCHAR(52);
DECLARE category_node VARCHAR(31);
BEGIN
  INSERT INTO salesforce.employee_interactions (
    "ei",
    "coaching_status_date",
    "coaching_issued",
    "approval_process",
    "coaching_status",
    "interaction_type",
    "interaction_status",
    "record_type",
    "location",
    "client",
    "owner",
    "employee_contact",
    "supervisor"
  ) VALUES (
    NEW."Name",
    NEW."Coaching_Status_Date__c",
    NEW."Coaching_Issued_to_Employee__c",
    NEW."Approval_Process__c",
    salesforce.get_coaching_status(NEW."Coaching_Status__c"),
    salesforce.get_interaction_type(NEW."Interaction_Type__c"),
    salesforce.get_interaction_status(NEW."Interaction_Status__c"),
    salesforce.get_record_type_id(NEW."RecordTypeId"),
    NEW."Location__c",
    NEW."Client__c",
    NEW."OwnerId",
    NEW."Employee_Contact__c",
    NEW."Supervisor__c"
  );

  IF NEW."RecordTypeId" IN (
    SELECT "type_id"
    FROM salesforce.record_type
    WHERE type LIKE '%Performance%'
  ) THEN
    INSERT INTO salesforce.performance_reviews (
      "employee_interaction",
      "start_date",
      "end_date",
      "review_date",
      "total_rating",
      "type_of_evaluation"
    ) VALUES (
      salesforce.last_ei_id(),
      NEW."Evaluation_From__c",
      NEW."Evaluation_To__c",
      NEW."Performance_Review_Date__c",
      NEW."Total_Rating__c",
      salesforce.get_type_of_evaluation(NEW."Type_of_Evaluation__c")
    );
  END IF;

  IF NEW."RecordTypeId" IN (
    SELECT "type_id"
    FROM salesforce.record_type
    WHERE type NOT IN ('Performance Review', 'Employee Interaction')
  ) THEN
    INSERT INTO salesforce.coachings (
      "employee_interaction",
      "date_of_infraction",
      "reason",
      "result",
      "type_of_warning",
      "director"
    ) VALUES (
      salesforce.last_ei_id(),
      NEW."Date_of_Infraction__c",
      NEW."Reason_for_Coaching__c",
      salesforce.get_coaching_result(NEW."Result__c"),
      salesforce.get_type_of_warning(NEW."Type_of_Warning__c"),
      NEW."Director__c"
    );

    IF NEW."Primary_Reason_for_Coaching_Category__c" IS NOT NULL THEN
      INSERT INTO salesforce.primary_coaching_reason_categories_coaching_reason_categories (
        "coaching",
        "category_reasons",
        "is_primary"
      ) VALUES (
        salesforce.last_coaching_id(),
        salesforce.get_coaching_category(NEW."Primary_Reason_for_Coaching_Category__c"),
        TRUE
      );
    END IF;

    IF NEW."Reason_for_Coaching_Category__c" IS NOT NULL THEN
      FOREACH category_node
        IN ARRAY
        STRING_TO_ARRAY(NEW."Reason_for_Coaching_Category__c", ',')
        LOOP
        INSERT INTO salesforce.primary_coaching_reason_categories_coaching_reason_categories (
          "coaching",
          "category_reasons",
          "is_primary"
        ) VALUES (
          salesforce.last_coaching_id(),
          salesforce.get_coaching_category(category_node),
          FALSE
        );
      END LOOP;
    END IF;

    IF NEW."RecordTypeId" IN (
      SELECT "type_id"
      FROM salesforce.record_type
      WHERE type LIKE '%Suspension%'
    ) THEN
      INSERT INTO salesforce.suspensions (
        "coaching",
        "shifts",
        "start",
        "end"
      ) VALUES (
        salesforce.last_coaching_id(),
        NEW."Suspended_Shifts__c",
        NEW."Suspension_Start__c",
        NEW."Suspension_End__c"
      );
    END IF;

    IF NEW."RecordTypeId" IN (
      SELECT "type_id"
      FROM salesforce.record_type
      WHERE type LIKE '%Termination%'
    ) THEN
      INSERT INTO salesforce.separations (
        "coaching",
        "date",
        "eligible_for_rehire",
        "category"
      ) VALUES (
        salesforce.last_coaching_id(),
        NEW."Separation_Date__c",
        NEW."Eligible_for_Rehire__c",
        salesforce.get_separation_category(NEW."Separation_Category__c")
      );

      IF NEW."Separation_Primary_Reason__c" IS NOT NULL THEN
        INSERT INTO salesforce.primary_separation_reasons_separation_reasons (
          "separation",
          "reason",
          "is_primary"
        ) VALUES (
          salesforce.last_separation_id(),
          salesforce.get_separation_reason(NEW."Separation_Primary_Reason__c"),
          TRUE
        );
      END IF;

      IF NEW."Separation_Reason__c" IS NOT NULL THEN
        FOREACH separation_node
          IN ARRAY
          STRING_TO_ARRAY(NEW."Separation_Reason__c", ',')
          LOOP
          INSERT INTO salesforce.primary_separation_reasons_separation_reasons (
            "separation",
            "reason",
            "is_primary"
          ) VALUES (
            salesforce.last_separation_id(),
            salesforce.get_separation_reason(separation_node),
            FALSE
          );
          END LOOP;
      END IF;
    END IF;
  END IF;

  RETURN NULL;
END;
$$;

CREATE TRIGGER employee_interaction__c_insert_trigger
  AFTER INSERT
  ON salesforce.employee_interaction__c
  FOR EACH ROW
  EXECUTE PROCEDURE salesforce.ei_trigger_func();
