/**
 * @site https://jestjs.io/docs/configuration
 */
import { InitialOptionsTsJest } from "ts-jest/dist/types";

export default async (): Promise<InitialOptionsTsJest> => ({
  preset: "ts-jest",
  testEnvironment: "node",
  roots: ['./src'],
  verbose: true,
  automock: true
});